# Self-Hosted "CodePen" alternative

## Usage

Just run

```bash
new_prototype <project_name> [-t <template>]
```

and it will copy things inside <code>[_templates](_templates)/default</code> into `./<project_name>`. 

This structure makes it easy to add new templates, just add them inside <code>[_templates](_templates)</code>.

## Serve

For local development just serve this whole repo for example with

```bash
live-server . >/dev/null &
```

and then deploy this repo to something like _Netlify_ for sharing.


