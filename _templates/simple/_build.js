
import fs from "fs/promises";
import hljs from 'highlight.js';

const generateHTML = ({ title, files }) => `
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- HighlightJS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.7.2/styles/github.min.css">

    <title>${title} | Prototype</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Inter:wght@400;600&display=swap');

        * {
            box-sizing: border-box;
        }

        html, body {
            height: 100%;
        }
        
        body {
            margin: 0;
            display: flex;
            
            font-size: 16px;

            font-family: 'Inter', sans-serif;
            font-weight: 400;
            
            background: #f8f8f8;
            color: #333;
        }
        
        h1, h2, h3, h4 {
            margin: 0;
            font-weight: 600;
        }

        .pane {
            width: 50%;
            height: 100%;

            overflow-y: auto;
        }

        .files {
            display: flex;
            flex-direction: column;

            padding: 1.5rem 1rem;

            align-items: stretch;
            gap: 1.5rem;

            border-right: 1px solid #e8e8e8;
        }

        .file {
            display: flex;
            flex-direction: column;
            align-items: stretch;

            border-radius: 4px;
            box-shadow: 0 0 6px 2px rgba(0, 0, 0, 0.2);

            background: #fff;
        }
        
        .file .name {
            padding: 0.25rem 0.5rem;
            
            background: #eee;
            border-bottom: 1px solid #ccc;

            border-top-left-radius: 4px;
            border-top-right-radius: 4px;
        }

        .file .content {
            padding: 0.5rem;

            overflow: auto;
        }

        pre, code {
            margin: 0;
            white-space: pre-wrap;

            font-family: 'Iosevka', 'Inconsolata', 'Menlo', monospace;
        }

        .preview {
            display: flex;
            flex-direction: column;

            align-items: stretch;

            padding: 1rem;
        }

        iframe {
            flex-grow: 1;

            border-radius: 4px;
            box-shadow: 0 0 9px rgba(0, 0, 0, 0.25);

            background: #fff;
        }

        @media (orientation: portrait) {
            body {
                flex-direction: column;
            }

            .pane {
                width: 100%;
                height: 50%;
            }

            .files {
                border-bottom: 1px solid #ddd;
            }
        }

        /* Mobile in Landscape & Portrait */

        @media screen
            and (max-device-width: 450px) and (max-device-height: 900px) {
            
            body {
                font-size: 12px;
            }
        }

        @media screen
            and (max-device-height: 450px) and (max-device-width: 900px) {
            
            body {
                font-size: 12px;
            }
        }
    </style>
</head>
<body>
    <div class="pane files">
        <h2>Prototype: ${title}</h2>
        ${files.map(file => `
            <div class="file">
                <div class="name">${file.name}</div>
                <div class="content"><pre><code>${hljs.highlight(file.content, { language: file.lang }).value}</code></pre></div>
            </div>
        `).join('\n')}
    </div>
    <div class="pane preview">
        <iframe src="../index.html" frameborder="0"></iframe>
    </div>
</body>
</html>
`;

async function generatePage({ title, files: inputs }) {
    const files = await Promise.all(inputs.map(async file => {
        return {
            ...file,
            content: (await fs.readFile(file.name, 'utf8')).trim(),
        };
    }));

    const html = generateHTML({ title, files });
    
    await fs.writeFile('code/index.html', html, 'utf8');
}

generatePage({
    title: 'Simple Template',
    files: [
        { name: 'index.html', lang: 'html' },
        { name: 'style.css', lang: 'css' },
        { name: 'index.js', lang: 'js' },
    ],
});

